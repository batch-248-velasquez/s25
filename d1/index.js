console.log("Hello World")


let sample1 = `
	{
		"name": "Cardo Dalisay",
		"age": 20,
		"address": {
			"city": "Quezon City",
			"country": "Philippines"
		}
	}

`;

console.log(sample1);

let person = `
	{
	"name": "Marv",
	"age": 15,
	"gender": "Male",
	"address": {
		"city": "Marquette",
		"state": "Michigan"
	}
`;

console.log(person)

let sample2 = `
	{
		"name": "Em Vee",
		"age": 20,
		"address": {
			"city": "Quezon City",
			"country": "Philippines"
		}
	}

`;
console.log(sample2);
console.log(typeof sample1);


console.log (JSON.parse(sample1));
console.log (JSON.parse(sample2));


let sampleArr =`
	[
		{
			"email": "sinayangmoko@gmail.com",
			"password": "february14",
			"isAdmin": "false"
		},
		{
			"email": "dalisay@gmail.com",
			"password": "thecountryman",
			"isAdmin": "false"
		},
		{
			"email": "jsonv@gmail.com",
			"password": "friday13",
			"isAdmin": "true"
		}
	]
`;

console.log(sampleArr);
console.log(typeof sampleArr);
let parsedSampleArr = JSON.parse(sampleArr);
console.log ([parsedSampleArr]);

console.log (parsedSampleArr.pop())
console.log(parsedSampleArr)

sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr)

let jsonArr = `
	[
	"pizza",
	"hamburger",
	"spag",
	"shang",
	"hotdog",
	"pancit"
	]
`;

let parsedJsonArr = JSON.parse(jsonArr);
console.log ([parsedJsonArr]);

console.log(parsedJsonArr.pop())
console.log(parsedJsonArr)

console.log(parsedJsonArr.push("palabok"))
console.log(parsedJsonArr)


jsonArr = JSON.stringify(parsedJsonArr);
console.log(jsonArr)



let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("What is your city?"),
	country: prompt("What is your country? ")
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address,
})

console.log(otherData)